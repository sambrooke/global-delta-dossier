
import geopandas as geopandas
import pandas as pd

def chunks(lst, n):
    """Yield successive n-sized chunks from lst."""
    for i in range(0, len(lst), n):
        yield lst[i:i + n]

global_avs = pd.read_csv('Final_database_avulsions_checked.csv')

section_html_list = ''''''
avulsion_links = []

already_used = []


for ridx, row in global_avs.iterrows():
    
    if (int(row.Year) > 0):
        av_id_no_year = row.Av_ID[:-5]

    if row.Av_ID in already_used:
        continue

    already_used.append(row.Av_ID)

    avulsion_links.append(f'''              <li><a href="#/{row.Av_ID}">{row.Av_ID}</a></li>''')

    section_html = f'''
            <section id="{row.Av_ID}">
              <h4>{row.Av_ID}</h4>
              <section>
                <a class='index-link' href="#/contents_page">Back to index</a>
                <img class='slope_figure' src="https://sambrooke.uk/data/images/profiles/{row.Av_ID}_all.png">
              </section>
              <section>
                <a class='index-link' href="#/contents_page">Back to index</a>
                <img class='wbmsed_figure' src="https://sambrooke.uk/data/images/wbmsed/{row.Av_ID}.jpg">
              </section>
              <section>
                <a class='index-link' href="#/contents_page">Back to index</a>
                <img class='wbmsed_discharge' src="https://sambrooke.uk/data/images/discharge/{row.Av_ID}_PEAKS.jpg">
              </section>
              <section>
                <table>
                    <thead>
                        <tr>
                            <td>
                                Key
                            </td>
                            <td>
                                Value
                            </td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                River Name
                            </td>
                            <td>
                                {row.River}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Tscour [months]
                            </td>
                            <td>
                                {row['Tscour [months]']}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Hydrosheds discharge [cms]
                            </td>
                            <td>
                                {row['Hydrosheds discharge [cms]']}
                            </td>
                        </tr>
                </table>
              </section>
            </section>
    '''

    section_html_list+=section_html

av_list_chunks = list(chunks(avulsion_links, 25))
contents_page = f'''
        <section id="contents_page">
            <div class="container">
'''

for c in av_list_chunks:
    contents_page += '''
                <div class="col">
                    <ul>'''
    for av in c:
        contents_page += f'''
        {av}'''

    contents_page +='''
                    </ul>
                </div>
    '''

contents_page += '''
        </div>
    </section>'''

start_page = '''
<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <title>reveal.js</title>

    <link rel="stylesheet" href="dist/reset.css">
    <link rel="stylesheet" href="dist/reveal.css">
    <link rel="stylesheet" href="dist/theme/black.css" id="theme">
    <link rel="stylesheet" href="custom_css/global_delta.css" id="theme">

    <!-- Theme used for syntax highlighted code -->
    <link rel="stylesheet" href="plugin/highlight/monokai.css" id="highlight-theme">
  </head>
  <body>
    <div class="reveal">
      <div class="slides">
        <section id="title_card">
          <h3>Global Delta Compilation v2</h3>
          <h4>Channel Slope</h4>
          <img src="https://sambrooke.uk/data/images/titlecard_delta_pemali.jpg" alt="" height="500px">
        </section>
'''

end_page = '''

      </div>
    </div>

    <script src="dist/reveal.js"></script>
    <script src="plugin/notes/notes.js"></script>
    <script src="plugin/markdown/markdown.js"></script>
    <script src="plugin/highlight/highlight.js"></script>
    <script>
      // More info about initialization & config:
      // - https://revealjs.com/initialization/
      // - https://revealjs.com/config/
      Reveal.initialize({
        hash: true,

        // Learn about plugins: https://revealjs.com/plugins/
        plugins: [ RevealMarkdown, RevealHighlight, RevealNotes ]
      });
    </script>
  </body>
 </html>
'''
final_page = start_page+contents_page+section_html_list+end_page

with open('channel_slopes.html', 'w') as f:
    f.write(final_page)
